<?php 
class General_model extends CI_Model {

	function __construct() {
	    parent::__construct();
	    $this->load->database(); 
	}

	public function get_fields_name($table){
		$fields = $this->db->list_fields($table);
		$field_arr = Array();
		foreach ($fields as $key => $value) {
			if($value != 'id'){
				$field_arr[] = $value;
			}
		}
		return $field_arr;
    }

	public function get_comments_name($table){
		$fields = $this->db->query('SHOW FULL COLUMNS FROM '.$table);
		$field_arr = Array();
		foreach ($fields->result_array() as $key => $value) {
			if(empty($value['Comment'])){
				$field_arr[$value['Field']]=$value['Field'];
			}else{
				$field_arr[$value['Field']]=$value['Comment'];				
			}
		}
		return $field_arr;
    }    	
	
	public function get_datas_all($table){
		$get_datas = $this->db->get($table);
		$fields = Array( 0 => "--Select--");
		foreach ($get_datas->result_array() as $key => $value) {
			$a = 0;
			$selected = '';
			foreach ($value as $key2 => $value2) {
				if($a == 1){
					$selected = $value2;
					break;
				}
				$a++;
			}
			$fields[$value['id']] = $selected;				
		}	
		return $fields;				
	}

	public function get_datas($table, $limit=10, $offset=0,$post=Array()){
		$comments = $this->get_comments_name($table);
		switch ($table){
			case '':
				
			break;
			default:
				if(empty($post['search'])){
					$get_datas = $this->db->get($table, $limit, $offset);
				}else{
					$fields = $this->db->query('SHOW FULL COLUMNS FROM '.$table);
					$field_arr = Array();
					foreach ($fields->result_array() as $key => $value) {
						$field_arr [] =	$value['Field']." LIKE '%".trim($post['search'])."%'";
					}
					$get_datas = $this->db->where(implode(' OR ', $field_arr))->get($table, $limit, $offset);
				}			
			break;
		}
		$get_datas_arr = Array();
		$array = Array();
		$first = true;
		foreach ($get_datas->result_array() as $key => $value) {
			if($first){
				$get_datas_selected_arr = Array();
				foreach ($value as $key2 => $value2) {	
					$get_datas_selected_arr[] = str_replace('_', ' ', $comments[$key2]);
				}
				$get_datas_selected_arr[] = 'delete action';
				$get_datas_arr[] = $get_datas_selected_arr;			
			}
			$first = false;
			$get_datas_selected_arr = Array();
			foreach ($value as $key2 => $value2) {	
				$get_datas_selected_arr[] = $value2;
			}
			$get_datas_selected_arr[] = 
			form_button('delete','Delete','onClick="if(confirm(\'Do you want to delete this record?\'))document.location.href=\''.site_url('/general/delete/'.$table.'/'.$value['id']).'\';"');			
			$get_datas_arr[] = $get_datas_selected_arr;			
		}
		return $get_datas_arr;		
	}

	public function get_datas_2($table, $limit=10, $offset=0,$table_name,$post){
		$comments = $this->get_comments_name($table);
		switch ($table){
			case '':
				
			break;
			default:
				if(empty($post['search'])){
					$get_datas = $this->db->get($table, $limit, $offset);
				}else{
					$fields = $this->db->query('SHOW FULL COLUMNS FROM '.$table);
					$field_arr = Array();
					foreach ($fields->result_array() as $key => $value) {
						$field_arr [] =	$value['Field']." LIKE '%".trim($post['search'])."%'";
					}
					$get_datas = $this->db->where(implode(' OR ', $field_arr))->get($table, $limit, $offset);					
				}
			break;
		}
		$get_datas_arr = Array();
		$array = Array();
		$first = true;
		foreach ($get_datas->result_array() as $key => $value) {
			if($first){
				$get_datas_selected_arr = Array();
				foreach ($value as $key2 => $value2) {	
					$get_datas_selected_arr[] = str_replace('_', ' ', $comments[$key2]);
				}
				$get_datas_selected_arr[] = 'select action';
				$get_datas_arr[] = $get_datas_selected_arr;			
			}
			$first = false;
			$get_datas_selected_arr = Array();
			foreach ($value as $key2 => $value2) {	
				$get_datas_selected_arr[] = $value2;
			}
			$fields = Array();
			unset($post["id_of_".$table]);
			foreach ($post as $key => $valuex) {
				$fields[] = $key." : '".$valuex."'";
			}
			$fields[] = "id_of_".$table." : '".$value['id']."'";
			$get_datas_selected_arr[] = 
			form_button('select','Select','onClick="post(\''.site_url('/general/load/'.$table_name).'\',{'.implode(",", $fields).'});"');			
			$get_datas_arr[] = $get_datas_selected_arr;			
		}
		return $get_datas_arr;		
	}

	public function insert_to_tables($data){
		$table_name = $data['table_name'];
		unset($data['submit']);
		unset($data['table_name']);
		$this->db->insert($table_name,$data);
	}   

	public function get_data_by_id($table_name,$id){
		return $this->db->get_where($table_name, array('id' => $id), 1, 0)->result_array()[0];
	}

	public function delete_tables($table_name,$id){
		$data = $this->get_data_by_id($table_name,$id);
		$this->db->delete($table_name, array('id' => $id));
		return $data;
	}

	public function get_count($table_name,$post=Array()){
		if(empty($post['search'])){
			return $this->db->count_all($table_name);
		}else{
			$fields = $this->db->query('SHOW FULL COLUMNS FROM '.$table_name);
			$field_arr = Array();
			foreach ($fields->result_array() as $key => $value) {
				$field_arr [] =	$value['Field']." LIKE '%".trim($post['search'])."%'";
			}
			$this->db->select('*');
			$this->db->from($table_name);			
			$this->db->where(implode(' OR ', $field_arr));
			return $this->db->count_all_results();					
		}

	}

}	
?>