<?php	
	$this->load->view('templates/header');
	echo form_open_multipart('general/save');
	$field_arr = Array();
	foreach ($fields as $key => $value) {
		$field_arr[] = $value.": $('#".$value."').val()";
	}
	foreach ($fields as $key => $value) {
		if(!isset($post[$value])){
			$post[$value] = '';
		}
		$label = str_replace('_', ' ', $comments[$value]);
		if($value == "time_created"){
			
		}else if(strpos($value, 'files') !== false){
			echo form_label($label.' : ').'<input type="file" name="'.$value.'" id="'.$value.'"><br>';
		}else if(strpos($value, 'id_of_') !== false){
			echo form_label($label.' : ').form_input($value, $post[$value],array(
											   'readonly' => 'readonly', 
				                               'id' => $value)
											).
			form_button('button', '...','onClick="
				post(\''.site_url('general/show_table/'.str_replace('id_of_', '', $value).'/'.$table_name).'\', {'.implode(",", $field_arr).'});"')."<br>";			
		}else{			
			echo form_label($label.' : ').form_input($value, $post[$value],array('id' => $value))."<br>";
		}
	}
	echo form_hidden('table_name', $table_name);
	echo form_submit('submit', 'Submit');
	echo form_button('view', 'View Data','onClick="document.location.href=\''.site_url('general/show/'.$table_name).'\';"');	
	echo form_close();
	$this->load->view('templates/footer');
?>