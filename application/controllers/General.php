<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {

    public function __construct(){
 		parent::__construct();    	
		$this->load->model('General_model');
		$this->load->library(array('table', 'pagination'));
      	$this->paging_config['first_link']       = 'First';
        $this->paging_config['last_link']        = 'Last';
        $this->paging_config['next_link']        = 'Next';
        $this->paging_config['prev_link']        = 'Prev';
        $this->paging_config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $this->paging_config['full_tag_close']   = '</ul></nav></div>';
        $this->paging_config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $this->paging_config['num_tag_close']    = '</span></li>';
        $this->paging_config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $this->paging_config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $this->paging_config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $this->paging_config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $this->paging_config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $this->paging_config['prev_tagl_close']  = '</span>Next</li>';
        $this->paging_config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $this->paging_config['first_tagl_close'] = '</span></li>';
        $this->paging_config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $this->paging_config['last_tagl_close']  = '</span></li>';		
    }

	public function load($table){
		$data['post'] = $this->input->post();
		$data['comments'] = $this->General_model->get_comments_name($table);
		$data['fields'] = $this->General_model->get_fields_name($table);		
		foreach ($data['fields'] as $key => $value) {
			if(strpos($value, 'id_of_') !== false){
eval("\$".$value." = \$this->General_model->get_datas_all('".str_replace('id_of_', '', $value)."');");		
eval("\$data['".$value."'] = \$".$value.";");
			}
		}		
		$data['table_name'] = $table;
		$this->load->view('general_view',$data);
	}

	public function show_table($table,$table_selected){
		session_start();
		$post = Array();
		if(empty($this->input->post())){
			$post = $_SESSION;
		}else{
			$post = $this->input->post();
			$_SESSION = $post;
		}
        $this->paging_config['base_url'] = site_url('general/show_table/'.$table."/".$table_selected); //site url
        $this->paging_config['total_rows'] = $this->General_model->get_count($table,$post); //total row
        $this->paging_config['per_page'] = 5;  //show record per halaman
        $this->paging_config["uri_segment"] = 5;  // uri parameter
        $this->paging_config["num_links"] = floor($this->paging_config["total_rows"] / $this->paging_config["per_page"]);
        $this->pagination->initialize($this->paging_config);
        $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$this->load->view('show_table_view',
			array('datas' => $this->General_model->get_datas_2($table,$this->paging_config["per_page"], $data['page'],$table_selected,$post),
				  'table_name' => $table,
				  'pagination' => $this->pagination->create_links(),
				  'post' => $post			
		));
	}

	public function show($table){
		$post = $this->input->post();
        $this->paging_config['base_url'] = site_url('general/show/'.$table); //site url
        $this->paging_config['total_rows'] = $this->General_model->get_count($table); //total row
        $this->paging_config['per_page'] = 5;  //show record per halaman
        $this->paging_config["uri_segment"] = 4;  // uri parameter
        $this->paging_config["num_links"] = floor($this->paging_config["total_rows"] / $this->paging_config["per_page"]);
        $this->pagination->initialize($this->paging_config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$this->load->view('show_view',
			array('datas' => $this->General_model->get_datas($table,$this->paging_config["per_page"], $data['page'],$post),
				  'table_name' => $table,
				  'pagination' => $this->pagination->create_links(),
				  'post' => $post						
		));
	}

	public function save()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$post = $this->input->post();	
			foreach ($_FILES as $key => $value) {		
				$config['allowed_types']        = '*';
				$config['upload_path']          = 'uploads';
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config);
				$post[$key] = "";
                if ($this->upload->do_upload($key)){
                	$post[$key] = $this->upload->data()["file_name"];
                }else{
                	echo ($this->upload->display_errors());
                	exit;
                }			
			}		
			$this->General_model->insert_to_tables($post);
			redirect(site_url('/general/load/'.$this->input->post('table_name')), 'refresh');	
		}
	}

	public function delete($table,$id){
		$data = $this->General_model->delete_tables($table,$id);		
		foreach ($data as $key => $value) {
			if(strpos($key, 'files') !== false){
				unlink('uploads/'.$value);
			}
		}			
		redirect(site_url('/general/show/'.$table), 'refresh');	
	} 	

}
