-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 29 Okt 2020 pada 11.49
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rmapd_database`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `first_table`
--

CREATE TABLE `first_table` (
  `id` int(11) NOT NULL,
  `id_of_second_table` int(11) NOT NULL,
  `first_column` varchar(255) NOT NULL,
  `files_column` varchar(255) NOT NULL,
  `second_files_column` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `first_table`
--

INSERT INTO `first_table` (`id`, `id_of_second_table`, `first_column`, `files_column`, `second_files_column`) VALUES
(1, 1, 'a', 'a', 'a');

-- --------------------------------------------------------

--
-- Struktur dari tabel `second_table`
--

CREATE TABLE `second_table` (
  `id` int(11) NOT NULL,
  `first_column` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `second_table`
--

INSERT INTO `second_table` (`id`, `first_column`) VALUES
(1, 'a'),
(2, 'b'),
(3, 'c');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `first_table`
--
ALTER TABLE `first_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_second_table` (`id_of_second_table`);

--
-- Indeks untuk tabel `second_table`
--
ALTER TABLE `second_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `first_table`
--
ALTER TABLE `first_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `second_table`
--
ALTER TABLE `second_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `first_table`
--
ALTER TABLE `first_table`
  ADD CONSTRAINT `first_table_ibfk_1` FOREIGN KEY (`id_of_second_table`) REFERENCES `second_table` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
